package com.test.movielist;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;

import com.test.movielist.Utils.AppPrefrences;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class SplashScreen extends AppCompatActivity {

    Handler handler;
    Runnable runnable;
    AppPrefrences appPrefrences;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        appPrefrences=new AppPrefrences(this);
        handler=new Handler();
        runnable=new Runnable() {
            @Override
            public void run() {
                if(appPrefrences.getLoggined()){
                    startActivity(new Intent(SplashScreen.this,MainActivity.class));
                    finish();
                }
                else {
                    startActivity(new Intent(SplashScreen.this,LoginActivity.class));
                    finish();
                }

            }

        };

        handler.postDelayed(runnable,3000);

    }
}
