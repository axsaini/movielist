package com.test.movielist.Network;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

import java.io.StringReader;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiService {
    public static ApiRequests apiRequests;
    public static ApiService apiService;


    public ApiService(){
        OkHttpClient okHttpClient=new OkHttpClient.Builder().addNetworkInterceptor(new StethoInterceptor()).build();

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiUrls.baseUrl).client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        apiRequests=retrofit.create(ApiRequests.class);



    }

    public static ApiRequests getInstance() {

        if (apiService==null){
            new ApiService();
        }

        return apiRequests;
    }

}
