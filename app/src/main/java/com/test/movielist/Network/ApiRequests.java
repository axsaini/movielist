package com.test.movielist.Network;

import com.test.movielist.Models.MovieListModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public interface ApiRequests {
    @GET("top_rated?api_key=8e0a6115beba2160f69b7c04584b39fd&language=en-US&page=1")
    Call<MovieListModel> getTopMovieList();

}
