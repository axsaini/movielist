package com.test.movielist;

import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.test.movielist.Models.MovieListModel;

import java.util.ArrayList;

public class ResultActivity extends AppCompatActivity {

    TextView title,description;
    ImageView poster;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        title=findViewById(R.id.title);
        description=findViewById(R.id.description);
        poster=findViewById(R.id.posterImage);
//        getActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
//get the attached extras from the intent
//we should use the same key as we used to attach the data.
        MovieListModel.Result result = intent.getExtras().getParcelable("result");
        Glide.with(this.getApplicationContext()).load("https://image.tmdb.org/t/p/w500"+result.getPosterPath()).into(poster);
        title.setText(result.getTitle());
        description.setText(result.getOverview());

    }
}
