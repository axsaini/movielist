package com.test.movielist.Fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.test.movielist.Adapter.MovieListAdapter;
import com.test.movielist.Interface.OnClickListener;
import com.test.movielist.Models.MovieListModel;
import com.test.movielist.Network.ApiRequests;
import com.test.movielist.Network.ApiService;
import com.test.movielist.Network.ApiUrls;
import com.test.movielist.R;
import com.test.movielist.ResultActivity;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MovieListFragment extends Fragment implements OnClickListener {
    ProgressBar progressBar;
    RecyclerView movieList;
    MovieListAdapter movieListAdapter;
    List<MovieListModel.Result> list;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        callApi();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        progressBar=view.findViewById(R.id.progress_circular);
        movieList=view.findViewById(R.id.movieList);
        list=new ArrayList<>();
        movieListAdapter=new MovieListAdapter(this);
        movieList.setLayoutManager(new LinearLayoutManager(getActivity()));
        movieList.setAdapter(movieListAdapter);
    }


    private void callApi() {

        ApiService.getInstance().getTopMovieList().enqueue(new Callback<MovieListModel>() {
            @Override
            public void onResponse(Call<MovieListModel> call, Response<MovieListModel> response) {
                progressBar.setVisibility(View.GONE);
                movieList.setVisibility(View.VISIBLE);
                movieListAdapter.update(response.body().getResults());
                list=response.body().getResults();
            }

            @Override
            public void onFailure(Call<MovieListModel> call, Throwable t) {

            }
        });

    }


    @Override
    public void onClick(int position) {

        Intent intent = new Intent(getActivity(), ResultActivity.class);

        intent.putExtra("result",list.get(position));

        getActivity().startActivity(intent);

    }
}
