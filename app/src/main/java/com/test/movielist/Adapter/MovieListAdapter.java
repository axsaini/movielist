package com.test.movielist.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.test.movielist.Fragments.MovieListFragment;
import com.test.movielist.Interface.OnClickListener;
import com.test.movielist.Models.MovieListModel;
import com.test.movielist.R;

import java.util.ArrayList;
import java.util.List;

public class MovieListAdapter extends RecyclerView.Adapter<MovieListAdapter.ViewHolder> {

    OnClickListener onClickListener;
    List<MovieListModel.Result> list;
    Context context;
    public MovieListAdapter(MovieListFragment movieListFragment) {
        onClickListener=movieListFragment;
        context=movieListFragment.getContext();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view= LayoutInflater.from(context).inflate(R.layout.movie_list_item,viewGroup,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        Glide.with(context).load("https://image.tmdb.org/t/p/w500"+list.get(i).getPosterPath()).into(viewHolder.poster);
        viewHolder.title.setText(list.get(i).getTitle());
        viewHolder.date.setText(list.get(i).getReleaseDate());

    }



    @Override
    public int getItemCount() {
        return list.size();
    }

    public void update(List<MovieListModel.Result> results) {
        this.list=results;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView poster;
        TextView title,date;
        ConstraintLayout layout;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            poster=itemView.findViewById(R.id.poster);
            title=itemView.findViewById(R.id.title);
            date=itemView.findViewById(R.id.date);
            layout=itemView.findViewById(R.id.lyt);
            layout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            onClickListener.onClick(getAdapterPosition());
        }
    }

}
