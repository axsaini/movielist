package com.test.movielist.Utils;

import android.content.Context;
import android.content.SharedPreferences;

public class AppPrefrences {
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    String isLoggined="is_logined";
    String userName="user_name";
    String userEmail="user_email";
    String photoUrl="photoUrl";
    public AppPrefrences(Context context){
    sharedPreferences=context.getSharedPreferences(
            "prefs", Context.MODE_PRIVATE);
    editor=sharedPreferences.edit();

    }

    public void setLoggined(boolean islogined){
        editor.putBoolean(isLoggined,islogined);
        editor.commit();
    }

    public boolean getLoggined(){
        return sharedPreferences.getBoolean(isLoggined,false);
    }

    public void setUserName(String un){
        editor.putString(userName,un);
        editor.apply();
    }
    public String getUserName(){
        return sharedPreferences
                .getString(userName,"");
    }

    public void setUserEmail(String un){
        editor.putString(userEmail,un);
        editor.apply();
    }
    public String getUserEmail(){
        return sharedPreferences
                .getString(userEmail,"");
    }

    public void setPhotoUrl(String un){
        editor.putString(photoUrl,un);
        editor.apply();
    }
    public String getPhotoUrl(){
        return sharedPreferences
                .getString(photoUrl,"");
    }


}
